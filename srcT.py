#!/usr/bin/python

from argparse import ArgumentParser
import sys

"""
% srcT : A simple tool for walking people through codes.
% Alejandro F Queiruga
% July 16, 2014

Don't read code, read a typeset pdf or a styled web page! Compile me
with
```bash
python srcT.py srcT.py -o README.md
pandoc README.md -o srcT.pdf
pandoc README.md -s -o srcT.html
```

Intro
=====

This is srcT, a code for rendering a python file with markdown
syntax embedded, so we have equations like $\int_{0}^{L}\nabla \delta
\mathbf{v} \cdot \nabla \mathbf{v} d\Omega$ inlined. The fundamendtal
idea is to transpose code with comments into document with code blocks.

The README.md file was created by running this script on itself.

Set up
======
Let's start by opening up an input and an output file. We want to
ignore everything before the first comment, where people (at least me)
like to put shebangs, imports and whatnot.



"""
parser = ArgumentParser(description="srcT. Convert python files into \
markdown.")
parser.add_argument('iname',nargs='?',default=None)
parser.add_argument('-o',nargs='?',default=None)
args = parser.parse_args()

in_file  = open(args.iname,"r") if args.iname else sys.stdin
out_file = open(args.o,"w") if args.o else sys.stdout

linenum=0
for line in in_file:
    linenum+=1
    if line=='"""\n' or line=='r"""\n':
        break
r"""
Processing
==========
Now, let's loop through the file, and write everything to the output
file. We keep track if we're in a code block or not to determine
whether or not to put an open block or a close block. Only
triple-quotes on lines by themselves are considered comments to be
typeset.    

Python by default parses escape sequences in strings, so a triplequote
with a bad escape, such as \\xi, $\xi$, will cause a crash. In this case, the
triple quote should be prefixed with an r to turn it into a raw string
literal, where escapes are not parsed. This is done by a r\"\"\"

"""
incode = False
for line in in_file:
    linenum+=1
    # Regular comments don't get typeset.
    if line=='"""\n' or line=='r"""\n':
        if incode:
            out_file.write( "```\n")
            incode = False
        else:
            out_file.write( '``` {{.python .numberLines startFrom="{0}"}}\n'.format(linenum))
            incode = True
    else:
        out_file.write(line)
"""
TODO: This logic might need some work, since there are situations where
triple quotes would be appear by themselves. 

Clean up
========

Now it's time to close the files. But we don't want to leave a
lingering open block, so we make sure to close the final code block if
one is open. If one wasn't open, then there would be a syntax error in
the python code, since there would be a triple-quote without a
matching member.


"""
if incode:
    out_file.write( "```\n")
    
in_file.close()
out_file.close()
"""

Try running it on this file! If you want to be clever, you can do some
io piping:
```bash
python srcT.py srcT.py | pandoc -o srcT.pdf
```
"""
